package com.md.stafftaxcostapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rank {
    BOSS("사장")
    ,HEAD_OF_DEPARTMENT("부장")
    ,DEPUTY_SECTION_CHIEF("대리");

    private final String rankName;
}
