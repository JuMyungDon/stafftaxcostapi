package com.md.stafftaxcostapi.entity;

import com.md.stafftaxcostapi.enums.Rank;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class StaffTax {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Rank rank;

    @Column(nullable = false)
    private LocalDate payDate;

    @Column(nullable = false)
    private Double preTax;

    @Column(nullable = false)
    private Double fourInsurances;

    @Column(nullable = false)
    private Double incomeTax;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
