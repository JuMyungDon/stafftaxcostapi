package com.md.stafftaxcostapi.controller;

import com.md.stafftaxcostapi.model.StaffTaxItem;
import com.md.stafftaxcostapi.model.StaffTaxRequest;
import com.md.stafftaxcostapi.model.StaffTaxResponse;
import com.md.stafftaxcostapi.service.StaffTaxService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/staff-tax")
public class StaffTexController {
    private final StaffTaxService staffTaxService;

    @PostMapping("/pay")
    public String setStaffTax(@RequestBody StaffTaxRequest request) {
        staffTaxService.setStaffTax(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<StaffTaxItem> getStaffTaxs() {
        return staffTaxService.getStaffTaxs();
    }

    @GetMapping("/detail/{id}")
    public StaffTaxResponse getStaffTax(@PathVariable long id) {
        return staffTaxService.getStafftax(id);
    }

    @PutMapping("/four-insurances/{id}")
    public String putFourInsurances(@PathVariable Long id, @RequestBody StaffTaxRequest request) {
        staffTaxService.putStaffTaxFourInsurances(id, request);

        return "ok";
    }

    @DeleteMapping("/{id}")
    public String delStaffTex(@PathVariable long id) {
        staffTaxService.delStaffTex(id);

        return "ok";
    }
}