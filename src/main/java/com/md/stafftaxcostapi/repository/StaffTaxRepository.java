package com.md.stafftaxcostapi.repository;

import com.md.stafftaxcostapi.entity.StaffTax;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffTaxRepository extends JpaRepository<StaffTax, Long> {
}
