package com.md.stafftaxcostapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaffTaxCostApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StaffTaxCostApiApplication.class, args);
    }

}
