package com.md.stafftaxcostapi.model;

import com.md.stafftaxcostapi.enums.Rank;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffTaxRequest {
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Rank rank;
    private Double preTax;
    private Double fourInsurances;
    private Double incomeTax;
    private String etcMemo;
}
