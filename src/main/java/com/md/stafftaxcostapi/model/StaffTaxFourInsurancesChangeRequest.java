package com.md.stafftaxcostapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffTaxFourInsurancesChangeRequest {
    private Double fourInsurances;
}
