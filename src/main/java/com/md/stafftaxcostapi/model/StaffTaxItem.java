package com.md.stafftaxcostapi.model;

import com.md.stafftaxcostapi.enums.Rank;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StaffTaxItem {
    private Long id;
    private String name;
    private String rank;
    private LocalDate payDate;
    private Double preTax;
    private Double fourInsurances;
    private Double incomeTax;
}
