package com.md.stafftaxcostapi.service;

import com.md.stafftaxcostapi.entity.StaffTax;
import com.md.stafftaxcostapi.model.StaffTaxItem;
import com.md.stafftaxcostapi.model.StaffTaxRequest;
import com.md.stafftaxcostapi.model.StaffTaxResponse;
import com.md.stafftaxcostapi.repository.StaffTaxRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StaffTaxService {
    private final StaffTaxRepository staffTaxRepository ;

    public void setStaffTax(StaffTaxRequest request) {
        StaffTax addData = new StaffTax();
        addData.setName(request.getName());
        addData.setRank(request.getRank());
        addData.setPayDate(LocalDate.now());
        addData.setPreTax(request.getPreTax());
        addData.setFourInsurances(request.getFourInsurances());
        addData.setIncomeTax(request.getIncomeTax());
        addData.setEtcMemo(request.getEtcMemo());

        staffTaxRepository.save(addData);
    }

    public List<StaffTaxItem> getStaffTaxs() {
        List<StaffTax> originList = staffTaxRepository.findAll();

        List<StaffTaxItem> result = new LinkedList<>();

        double afterPay = 0D;

        for(StaffTax staffTax : originList) {
            StaffTaxItem addItem = new StaffTaxItem();
            addItem.setId(staffTax.getId());
            addItem.setName(staffTax.getName());
            addItem.setRank(staffTax.getRank().getRankName());
            addItem.setPayDate(staffTax.getPayDate());
            addItem.setPreTax(staffTax.getPreTax());
            addItem.setFourInsurances(staffTax.getFourInsurances());
            addItem.setIncomeTax(staffTax.getIncomeTax());
            result.add(addItem);
        }
        return result;
    }

    public StaffTaxResponse getStafftax(long id) {
        StaffTax originData = staffTaxRepository.findById(id).orElseThrow();

        StaffTaxResponse response = new StaffTaxResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setRank(originData.getRank().getRankName());
        response.setPayDate(originData.getPayDate());
        response.setPreTax(originData.getPreTax());
        response.setFourInsurances(originData.getFourInsurances());
        response.setIncomeTax(originData.getIncomeTax());
        response.setAfterPay(originData.getPreTax() - originData.getFourInsurances() - originData.getIncomeTax());

        return response;
    }

    public void putStaffTaxFourInsurances(Long id, StaffTaxRequest request) {
        StaffTax originData = staffTaxRepository.findById(id).orElseThrow();
        originData.setFourInsurances(request.getFourInsurances());

        staffTaxRepository.save(originData);

    }

    public void delStaffTex(Long id) {
        staffTaxRepository.deleteById(id);
    }
}
